package setupInfo.standardInfo;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class PackageInfoLoader {
	
	
	public void packageInfoFillDefault(WebDriver driver) {
		
		
		
		
	}
	
	
	
	
	
	public void bookingPeriodDefault(WebDriver driver) {
		
		new Select(driver.findElement(By.id("utilizeToDate_Year_ID"))).selectByVisibleText("2017");
		new Select(driver.findElement(By.id("bookingToDate_Year_ID"))).selectByVisibleText("2017");
		
	}
	
	public void destinationDefault(WebDriver driver) {
		
		driver.findElement(By.id("add_destination_info")).click();
		
		driver.switchTo().frame("dialogwindow");
		
		driver.findElement(By.id("countryName")).clear();
		driver.findElement(By.id("countryName")).sendKeys("Germany");
		
		
		driver.findElement(By.id("countryName_lkup")).click();
		driver.switchTo().defaultContent();
		lookupReader("Germany", driver);
		driver.switchTo().frame("dialogwindow");
		
		driver.findElement(By.id("cityName")).clear();
		driver.findElement(By.id("cityName")).sendKeys("Berlin");
		
		
		driver.findElement(By.id("cityName_lkup")).click();
		driver.switchTo().defaultContent();
		lookupReader("Berlin", driver);
		driver.switchTo().frame("dialogwindow");
		
		((JavascriptExecutor)driver).executeScript("saveData()");
		
		driver.switchTo().defaultContent();
		
		
		
		
	}
	
	public void contactDefault(WebDriver driver) {
		
		
		((JavascriptExecutor)driver).executeScript("parent.showDialogWindow('DMCPackageCommonEntryPage.do?actionType=viewContactInfoPage&event=add&index=0', 630, 315, 'new'); ");
		
		driver.switchTo().frame("dialogwindow");
		driver.findElement(By.id("contactname")).clear();
		driver.findElement(By.id("contactname")).sendKeys("Richard Bennet");
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys("kavitha@colombo.rezgateway.com");
		
		new Select(driver.findElement(By.id("contact_type_select"))).selectByVisibleText("Reservations");
		
		((JavascriptExecutor)driver).executeScript("saveData()");
		
		driver.switchTo().defaultContent();
		
		
		
		
		
	}
	
	
	public void lookupReader(String stringValue,WebDriver driver) {
		
		driver.switchTo().frame("lookup");

		int optioncount=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).size();
		boolean optionloop=true;
		for (int j = 0;  optionloop; j++) {
			
			String lookuptext=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).get(j).findElement(By.tagName("td")).getText();
			if (lookuptext.contains(stringValue)) {
				
				driver.findElement(By.className("rezgLook"+j)).click();
				
				optionloop=false;
				
				
			}
		}
		
		driver.switchTo().defaultContent();	
		
	}

}
