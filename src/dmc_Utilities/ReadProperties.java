package dmc_Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class ReadProperties {
	
	static FileInputStream fs = null;
	static Properties prop = new Properties();
	
	public static HashMap<String, String> readpropeties(String Locator) throws IOException {

		HashMap<String, String> mymap = null;

		try {
			fs = new FileInputStream(new File(Locator));
			prop.load(fs);
			mymap = new HashMap<String, String>((Map) prop);
			return mymap;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return mymap;

	}
	
	

}
