package dmc_Utilities;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class dmc_Utilities_ReportsGenerator {
	int TestCaseCount;
	int passcount;
	int failcount;


	public StringBuffer verifyTrue(String Expected,String Actual,String Message)
	{
		StringBuffer PrintWriter=new StringBuffer();
		TestCaseCount++;
		System.out.println(TestCaseCount);
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");

		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(Actual.trim().equalsIgnoreCase(Expected.trim())){
				PrintWriter.append("<td class='Passed'>PASS</td>");
				passcount++;
			}
			else{
				PrintWriter.append("<td class='Failed'>FAIL</td>");
				failcount++;
			}

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}

		return PrintWriter;
	}
	
	

	public StringBuffer verifyTrueNumeric(int Expected,int Actual,String Message)
	{
		TestCaseCount++;
		StringBuffer PrintWriter=new StringBuffer();
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		//	TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(((Expected-1)<=Actual) && (Actual<=(Expected+1))){
				PrintWriter.append("<td class='Passed'>PASS</td>");
			}
			else{
				PrintWriter.append("<td class='Failed'>FAIL</td>");
			}

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}

		return PrintWriter;
	}

	
	public StringBuffer verifyTrueNumericDouble(double Expected,double Actual,String Message) {


		TestCaseCount++;
		StringBuffer PrintWriter=new StringBuffer();
		PrintWriter.append("<tr><td>"+TestCaseCount+"</td><td>"+Message+"</td>");
		//	TestCaseCount++;
		PrintWriter.append("<td>"+Expected+"</td>");
		PrintWriter.append("<td>"+Actual +"</td>");
		try {

			if(((Expected-1)<=Actual) && (Actual<=(Expected+1))){
				PrintWriter.append("<td class='Passed'>PASS</td>");
			}
			else{
				PrintWriter.append("<td class='Failed'>FAIL</td>");
			}

			PrintWriter.append("<td></td></tr>");
		} catch (Exception e) {
			PrintWriter.append("<td>"+e.toString()+"</td></tr>");
		}

		return PrintWriter;


	}
	

	public StringBuffer ReportHeaderFormatter() {

		StringBuffer  PrintWriter = new StringBuffer() ;

		PrintWriter.append("<html class='report'><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"> </head>");
		PrintWriter.append("<div class='header'><img height=\"19px\" src=\"menu_rez.png\"><p class='Hedding1'>Fixed Package Test Scenarios-</p>");
		PrintWriter.append("<br/>");
		PrintWriter.append("<body>");
		PrintWriter.append("<br><br>");



		return PrintWriter;


	}
	

	public StringBuffer ReportFooterFormatter() {
		StringBuffer  PrintWriter = new StringBuffer() ;
		PrintWriter.append("</body></html>");
		return PrintWriter;
	}
	

	public String reportwrite() {

		DateFormat date=new SimpleDateFormat("dd-MMM-yyyy");
		Date todate=Calendar.getInstance().getTime();
		String Dateinformat=date.format(todate);

		// File Location = new File("../Reports/"+Dateinformat+"\\");
		//File ParentLoc=new File("Reports/");
		File Location = new File("Reports/"+Dateinformat);
		// File Location = new File("Reports/");

		// If Parent Folder doesnt exists
		/*if (!ParentLoc.exists()) {
			 System.out.println("Creating Reports Folder");
			try {
				ParentLoc.m
			} catch (Exception e) {
				// TODO: handle exception
			}
		}*/

		// if the directory does not exist, create it
		if (!Location.exists()) {
			System.out.println("creating directory: " + Location);
			boolean result = false;

			try{
				Location.mkdirs();
				result = true;
			} catch(SecurityException se){
				//handle it
			}        
			if(result) {    
				System.out.println("DIR created");  
			}
		}

		String loc="Reports/"+Dateinformat+"/";
		return loc;

	}


}
