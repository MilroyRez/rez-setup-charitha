package dmc_Utilities;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import setup.com.utill.ExtentReportTemplate;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_Scenario;

public class login {
	private  WebDriver driver = null;
	private  String dcLogin="KavithaP";
	private	 String toComLogin="Altour";
	private Logger logger;
	
	
	public WebDriver driverinitialize() throws Exception {
		

ProfilesIni profile = new ProfilesIni();
 
/*FirefoxProfile myprofile = profile.getProfile(PropertySingleton.getInstance().getProperty("firefox.profile.path"));
 
WebDriver driver = new FirefoxDriver(myprofile);
		
		File profile = new File(PropertySingleton.getInstance().getProperty("firefox.profile.path"));*/
		
		//firefox.profile.path
	//	FirefoxProfile prof = new FirefoxProfile(profile);

/*DesiredCapabilities dc=DesiredCapabilities.firefox();
FirefoxProfile profile1 = new FirefoxProfile();
dc.setCapability(FirefoxDriver.PROFILE, profile1);
 driver =  new FirefoxDriver(dc);*/
	//	driver = new FirefoxDriver(myprofile);
 
 System.setProperty("webdriver.chrome.driver", "E:/Drivers/chromedriver.exe"); 
 WebDriver driver = new ChromeDriver(); 
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    
	    return driver;
		
	}
	
	public void loginTotheSystem(WebDriver driver,String url) throws InterruptedException {
		driver.manage().window().maximize();
		
		driver.get(url+"/admin/common/LoginPage.do");
		
		driver.findElement(By.id("user_id")).clear();
		driver.findElement(By.id("user_id")).sendKeys("rezuser");
		
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("rezuser123");
		
		Thread.sleep(2000);
		
		driver.findElement(By.id("loginbutton")).click();
		
		
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("MainModuleMenuItem_hotels")));
			driver.findElement(By.id("MainModuleMenuItem_hotels"));
			ExtentReportTemplate.getInstance().addtoReport("Login function ","User Should Logged in", "Logged in",true);
			//logger.info("Logged in to the system....");

		} catch (Exception ex) {
			logger.fatal("Login failed");
			ExtentReportTemplate.getInstance().addtoReport("Login function ","User Should Logged in", "Logged in Failed",false);
			driver.quit();
			System.exit(1);

		}
		
		
		
		
	}
	
	
	public void partnerLogin(WebDriver driver, dmc_Reservations_DataObjects_Scenario scenario) throws InterruptedException {
		
		driver.manage().window().maximize();
		
		if (scenario.getBookingChannel().contains("CC")) {
			
			driver.get(scenario.getUrl()+"/admin/common/LoginPage.do");
			
			driver.findElement(By.id("user_id")).clear();
			driver.findElement(By.id("user_id")).sendKeys("KavithaP");
			
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys("123456");
			
			Thread.sleep(2000);
			
			driver.findElement(By.id("loginbutton")).click();
			
			
			try {
				driver.findElement(By.id("mainmenurezglogo"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			
			
		}else {
			
			if (scenario.getPartnerType().contains("DC")) {
				
				driver.get(scenario.getUrl()+"Reservations/");
				
			}
			
		}
		
		
		
	}
	
	public static void main(String[] args) throws Exception {
		
		WebDriver driver;
		login newlogin=new login();
		
		 String chromeFilePath = System.getProperty("user.dir") + File.separator + "target" + File.separator + "chromedriver.exe";
		System.out.println(chromeFilePath);
		/*driver=newlogin.driverinitialize();
		newlogin.loginTotheSystem(driver,"http://dev3.rezg.net/rezbase_v3");*/
		
	}
	
	

}
