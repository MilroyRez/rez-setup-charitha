package dmc_Setup_PackageSetup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class dmc_Setup_PackageSetup_SetupSupports {
	
public void lookupReader(String stringValue,WebDriver driver) {
	
		
		driver.switchTo().frame("lookup");

		int optioncount=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).size();
		boolean optionloop=true;
		for (int j = 0;  optionloop; j++) {
			
			String lookuptext=driver.findElement(By.id("lookupDataArea")).findElement(By.tagName("table")).findElements(By.tagName("tr")).get(j).findElement(By.tagName("td")).getText();
			if (lookuptext.contains(stringValue)) {
				
				driver.findElement(By.className("rezgLook"+j)).click();
				
				optionloop=false;
				
				
			}
		}
		
		driver.switchTo().defaultContent();	
		
	}
	
	

}
