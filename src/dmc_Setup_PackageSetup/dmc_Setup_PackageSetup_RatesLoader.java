package dmc_Setup_PackageSetup;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import dmc_Setup_DataObjects.dmc_Setup_DataObjects_DMCPackageObject;
import dmc_Setup_DataObjects.dmc_Setup_DataObjects_RatesObject;
import setup.com.utill.ExtentReportTemplate;
public class dmc_Setup_PackageSetup_RatesLoader {
	
	
	 private Map<String, String> propertyset = null;
	   
	   
	   public dmc_Setup_PackageSetup_RatesLoader(Map<String, String> propertyset) {
		this.propertyset = propertyset;
	}


	dmc_Setup_PackageSetup_SetupSupports supportClass=new dmc_Setup_PackageSetup_SetupSupports();

	public void ratesLoad(dmc_Setup_DataObjects_DMCPackageObject obj, WebDriver driver) throws InterruptedException {



		int numberofrateContracts=obj.getRatelist().size();
		
		System.out.println("numberofrateContracts"+numberofrateContracts);

		for (int i = 0; i <numberofrateContracts; i++) {


			dmc_Setup_DataObjects_RatesObject rate=new dmc_Setup_DataObjects_RatesObject();

rate=obj.getRatelist().get(i);

			driver.get(propertyset.get("Hotel.BaseUrl")+"/dmc/setup/DMCPackageAddInventoryRatePage.do?actionType=displayRatePage");

			driver.findElement(By.id("packageName")).clear();
			driver.findElement(By.id("packageName")).sendKeys(obj.getStandardInfo().getPackageinfo().getPackageName());

			driver.findElement(By.id("packageName_lkup")).click();
			driver.switchTo().defaultContent();
			supportClass.lookupReader(obj.getStandardInfo().getPackageinfo().getPackageName(), driver);


			driver.findElement(By.id("noOfPackageDays")).getText();
			
			new Select(driver.findElement(By.id("periodTo_Day_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getBookingPeriodTo().split("-")[0]);
			new Select(driver.findElement(By.id("periodTo_Month_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getBookingPeriodTo().split("-")[1]);
			new Select(driver.findElement(By.id("periodTo_Year_ID"))).selectByVisibleText(obj.getStandardInfo().getPackageinfo().getBookingPeriodTo().split("-")[2]);



			new Select(driver.findElement(By.id("pax_grp_select"))).selectByVisibleText(rate.getPaxGroup());

			for (int j = 0; j < 7; j++) {
try {
	driver.findElement(By.id("flightRateArray_"+j)).clear();
	driver.findElement(By.id("flightRateArray_"+j)).sendKeys(rate.getFlightrate());

} catch (Exception e) {
	// TODO: handle exception
}
			
				driver.findElement(By.id("singleRoomRateArray_"+j)).clear();
				driver.findElement(By.id("singleRoomRateArray_"+j)).sendKeys(rate.getSingleRoomRate());
				driver.findElement(By.id("twinRoomRateArray_"+j)).clear();
				driver.findElement(By.id("twinRoomRateArray_"+j)).sendKeys(rate.getTwinRoomRate());
				driver.findElement(By.id("tripleRoomRateArray_"+j)).clear();
				driver.findElement(By.id("tripleRoomRateArray_"+j)).sendKeys(rate.getTripleRoomRate());
				try {
					driver.findElement(By.id("childWithoutBedArray_"+j)).clear();
					driver.findElement(By.id("childWithoutBedArray_"+j)).sendKeys(rate.getChildWithoutBedRate());
				
				} catch (Exception e) {
					// TODO: handle exception
				}
				
				
					driver.findElement(By.id("childWithBedArray_"+j)).clear();
				driver.findElement(By.id("childWithBedArray_"+j)).sendKeys(rate.getChildWithBedRate());
				driver.findElement(By.id("infantSupplementArray_"+j)).clear();
				driver.findElement(By.id("infantSupplementArray_"+j)).sendKeys(rate.getInfantSupplement());
				

			
					if (rate.getLandComponentTaxBy().contains("P")) {
						driver.findElements(By.id("taxValByVal_"+j)).get(0).click();
					}else {
						driver.findElements(By.id("taxValByVal_"+j)).get(1).click();
					}


					driver.findElement(By.id("taxValueAmountArray_"+j)).clear();
				driver.findElement(By.id("taxValueAmountArray_"+j)).sendKeys(rate.getLandComponentTaxValue());


				
			}
			
			System.out.println(rate.getPaxGroup());
Thread.sleep(1000);
			((JavascriptExecutor)driver).executeScript("submitData();");
			Thread.sleep(4000);
			
			WebDriverWait wait =  new WebDriverWait(driver, 60);
			try {
				
				
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("dialogMsgText")));
			    System.out.println(driver.findElement(By.id("dialogMsgText")).isDisplayed());
			   
			    if(driver.findElement(By.id("dialogMsgText")).isDisplayed()){
			    	
			    	String TextMessage = driver.findElement(By.id("dialogMsgText")).getText();
					System.out.println(TextMessage);
					
					if(TextMessage.equalsIgnoreCase("Data Saved Successfully") ){
						
						ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Rates page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Rates should be saved", TextMessage , true);
						
						driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
						}
						else {
							
							ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Rates page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Rates should be saved", TextMessage , false);
							driver.findElement(By.xpath(".//*[@id='dialogMsgActionButtons']/a/img")).click();
						}
			    }
			    else{
			    	
			    	String TextMessage = driver.findElement(By.id("MainMsgBox_msgDisplayArea")).getText();
					System.out.println(TextMessage);
					
					ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Rates Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Rates  should be saved", TextMessage , false);
					
					driver.findElement(By.xpath(".//*[@id='MainMsgBox']/table/tbody/tr[1]/td/table/tbody/tr/td[4]/a/img")).click();
			    }
				
			} catch (Exception e) {
				
				ExtentReportTemplate.getInstance().addtoReport("dmc_Setup_PackageSetup_Rates page Saving function "+obj.getStandardInfo().getPackageinfo().getPackageName(), "dmc_Setup_PackageSetup_Rates should be saved", e.toString() , false);
				
			}
			
		}




	}

}
