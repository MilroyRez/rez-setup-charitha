package dmc_Reservations_DataObjects;

public class dmc_Reservations_DataObjects_OccupancyObject {
	
	
	String roomNo;
	String adultCount;
	String childCount;
	String infantCount;
	
	
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	public String getChildCount() {
		return childCount;
	}
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	public String getInfantCount() {
		return infantCount;
	}
	public void setInfantCount(String infantCount) {
		this.infantCount = infantCount;
	}
	
	

}
