package dmc_Reservations_ReservationFlowReaders;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_Scenario;
import dmc_Reservations_DataObjects.dmc_Reservations_DataObjects_SearchObject;
import dmc_Utilities.dmc_Utilities_ReportsGenerator;
import dmc_Utilities.login;
import dmc_inputdataLoader.dmc_inputdataLoader_ReservationObjectReader;

public class dmc_Reservations_ReservationFlowReaders_bookingEngineReaders {
	int numberofCountriesinthedropdown=0;
	StringBuffer reportstream=new StringBuffer();
	ArrayList<String> durationmontharray=new ArrayList<String>();
	ArrayList<String> durationmontharrayExpected=new ArrayList<String>();
	
	dmc_Utilities_ReportsGenerator report=new dmc_Utilities_ReportsGenerator();
	
	public void bookingEngineLoader(WebDriver driver,  dmc_Reservations_DataObjects_Scenario scenario) throws InterruptedException {
		
		dmc_Reservations_DataObjects_SearchObject search=new dmc_Reservations_DataObjects_SearchObject();
		search=scenario.getSearch();

		try {
			driver.switchTo().frame("bec_container_frame");
		} catch (Exception e) {
			// TODO: handle exception
		}

		driver.findElement(By.id("pLi")).click();
		
		if (scenario.getBookingChannel().contains("web")) {
			driver.findElement(By.className("FixedPackages")).click();
			
		}else {
			driver.findElement(By.id("FixedPackages")).click();
			

		}

		WebDriverWait driverWait1 = new WebDriverWait(driver, 60);
		driverWait1.until(ExpectedConditions.presenceOfElementLocated(By.id("search_btns_p")));


		//-----country 
//		try {
//			numberofCountriesinthedropdown=new Select(driver.findElement(By.id("P_Country"))).getOptions().size();
//			driver.findElement(By.id("P_Country")).sendKeys("A");
//			new Select(driver.findElement(By.id("P_Country"))).selectByVisibleText(search.getCountryOfResidence());
//			new Select(driver.findElement(By.id("P_Country"))).selectByVisibleText(search.getCountryOfResidence());

//		} catch (Exception e) {
			// TODO: handle exception
//		}


//		Thread.sleep(5000);

		//-flight inclusive


		if (search.getFlightInclusive().contains("N")) {

			driver.findElement(By.id("Package_Type1")).click();
			Thread.sleep(4000);
			
			driver.findElement(By.id("package_Ex_des")).sendKeys(search.getDestination());

			((JavascriptExecutor)driver).executeScript("$('#hid_package_Ex_des').val('"+search.getDestinationValue()+"');");

			if (!(driver.findElement(By.id("hid_package_Ex_des")).getAttribute("value").equalsIgnoreCase(search.getDestinationValue()))) {
				((JavascriptExecutor)driver).executeScript("$('#hid_package_Ex_des').val('"+search.getDestinationValue()+"');");
			}



		} else{

			driver.findElement(By.id("Package_Type2")).click();

			driver.findElement(By.id("package_Loc_ori")).sendKeys(search.getOrigin());
			((JavascriptExecutor)driver).executeScript("document.getElementById('hid_package_Loc_ori').setAttribute('value','"+search.getOriginValue()+"');");


			driver.findElement(By.id("package_Loc_des")).sendKeys(search.getDestination());
			((JavascriptExecutor)driver).executeScript("document.getElementById('hid_package_Loc_des').setAttribute('value','"+search.getDestinationValue()+"');");




		}



		//-Month
	driver.findElement(By.id("Travel_On")).click();
	
	Select monthSelect=new Select(driver.findElement(By.id("Travel_On")));
	List<WebElement> optionlist=monthSelect.getOptions();
	
for (int i = 0; i < optionlist.size(); i++) {
	System.out.println(optionlist.get(i).getText());
	durationmontharray.add(i, optionlist.get(i).getText());
	
}


		/*Select month=new Select(driver.findElement(By.id("Travel_On")));
		month.deselectByVisibleText("Any");
		month.selectByVisibleText(searchCriteria.getDepartureMonth());

		//-Duration


		driver.findElement(By.id("No_of_Days")).click();
		snap.Snapshotreporter(driver,searchCriteria.getTestId(),"Package Duration Load");
		new Select(driver.findElement(By.id("No_of_Days"))).selectByVisibleText(searchCriteria.getDuration().trim());
		//-Package Type

		if (searchCriteria.getBookingChannel().equalsIgnoreCase("web")){

			driver.findElement(By.partialLinkText("Show Additional Search Options")).click();

		}

		driver.findElement(By.id("P_Type")).click();
		snap.Snapshotreporter(driver,searchCriteria.getTestId(),"Package Duration Load");
		try {
			new Select(driver.findElement(By.id("P_Type"))).selectByVisibleText(searchCriteria.getPackageType());

		} catch (Exception e) {
			System.out.println("!!!!!!!!!!!!Package type selection error"+e);
		}

		
		if (searchCriteria.getFlightStatus().equalsIgnoreCase("Excluding Flight ")) {

			driver.findElement(By.id("Package_Type1")).click();
			Thread.sleep(4000);
			System.out.println(searchCriteria.getArrival());
			driver.findElement(By.id("package_Ex_des")).sendKeys(searchCriteria.getArrival());

			((JavascriptExecutor)driver).executeScript("$('#hid_package_Ex_des').val('"+searchCriteria.getArrivalValue()+"');");

			if (!(driver.findElement(By.id("hid_package_Ex_des")).getAttribute("value").equalsIgnoreCase(searchCriteria.getArrivalValue()))) {
				((JavascriptExecutor)driver).executeScript("$('#hid_package_Ex_des').val('"+searchCriteria.getArrivalValue()+"');");
			}


			//((JavascriptExecutor)driver).executeScript("document.getElementById('hid_package_Ex_des').setAttribute('value','"+searchCriteria.getArrivalValue()+"');");

		}*/

		driver.findElement(By.id("search_btns_p")).click();
		driver.switchTo().defaultContent();


		Thread.sleep(5000);

		
		




		
		
		
	}
	
	public ArrayList<String> durationExpectedGenerate() {
		
		DateFormat date=new SimpleDateFormat("dd-MMM-yyyy");
		Date todate=Calendar.getInstance().getTime();
		
		String dateinformat=new SimpleDateFormat("MMMM-yyyy").format(todate);
		System.out.println("dateinformat"+dateinformat);
		
		for (int i = 0; i < 12; i++) {
			
			System.out.println(Calendar.getInstance().get(Calendar.MONTH)+i);
			
		}
		return durationmontharrayExpected;
	}
	
	
	public StringBuffer bookingenginertestreport() {
		
		
		
				reportstream.append("</br></br>");
				reportstream.append("</br></br><table ><tr><th>Test Case #</th><th>Test Description</th><th>Expected Result</th><th>Actual Result</th><th>Test Status</th></tr>");

				reportstream.append("<tr><td colspan=5></td></tr>");
		

				reportstream.append(report.verifyTrueNumeric(numberofCountriesinthedropdown, 245, "Country drop down-The country Count"));
	
				for (int i = 0; i < durationmontharray.size(); i++) {
					
					reportstream.append(report.verifyTrueNumeric(numberofCountriesinthedropdown, 245, "Country drop down-The country Count"));

					
				}
	
		
				reportstream.append("</table>");


		
		
		return reportstream;
		
	}

	
	

}

